const renderMovie = (movie) => {
  const movie_card =  document.createElement('div')
  movie_card.className += "movie"
  
  // image
  const img = document.createElement('img')
  img.src = movie.Poster
  img.className += 'movie-poster'
  movie_card.append(img)
  
  // title
  const title = document.createElement('h2')
  title.textContent = movie.Title
  title.className += 'movie-title'
  movie_card.append(title)

  // year of relise
  const relise_year = document.createElement('p')
  relise_year.textContent = 'utgitt: ' + movie.Year
  relise_year.className += 'movie-relise'
  movie_card.append(relise_year)

  return movie_card
} 

const fomdb = (apikey) => fetch('http://www.omdbapi.com/?apikey=' + apikey + 's=' + search_term + '&plot=full')
  .then((result) => result.json())
  .then((json) => {for (movie in json.Search) {
    console.log(json.Search[movie])
    document.getElementById('response')
    .append(renderMovie(json.Search[movie]))
  }})
  .catch((error) => console.error(error))

const search_term = 'day'


fetch('./resurser.json')
  .then(response => response.json())
  .then(apikeys => fomdb(apikeys.omdbapi))
  .catch(err => console.error(err));